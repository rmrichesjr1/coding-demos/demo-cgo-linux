This demo-cgo-linux project consists of demos of coding with CGo to access low-level Linux system calls and perhaps other library functions.

demo-cgo-linux is licensed under the GNU General Public License, version 2 or any later version.  See file LICENSE for details.
