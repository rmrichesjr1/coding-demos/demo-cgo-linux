// chown.go

// This simple program shows how to use cgo to imitate the chown command.
//
// The demo-cgo-linux project is licensed under the GNU General Public
// License, version 2 or any later version.  See file LICENSE for details.

package main

//////////////////////////////////////////////////////////////////////////////

// The inner C Code called by the outer Golang code:

// #include <errno.h>
// #include <grp.h>
// #include <pwd.h>
// #include <stdio.h>
// #include <stdlib.h>
// #include <string.h>
// #include <sys/types.h>
// #include <unistd.h>
//
// /* Return a uid or -1: */
// int userNameToUid(const char * userName) {
//     /* printf( "C DEBUG: user name: %s\n", userName); */
//     /* fflush(stdout); */
//     struct passwd * structP = getpwnam(userName);
//     if (NULL == structP) {
//         return(-1);
//     }
//     else {
//         int theUid = structP->pw_uid;
//         /* printf( "C DEBUG: uid: %d\n", theUid); */
//         /* fflush(stdout); */
//         /* Man page says to not attempt to free the struct. */
//         return(theUid);
//     }
// }
//
// /* Return a gid or -1: */
// int groupNameToGid(const char * groupName) {
//     /* printf( "C DEBUG: group name: %s\n", groupName); */
//     /* fflush(stdout); */
//     struct group * structP = getgrnam(groupName);
//     if (NULL == structP) {
//         return(-1);
//     }
//     else {
//         int theGid = structP->gr_gid;
//         /* printf( "C DEBUG: gid: %d\n", theGid); */
//         /* fflush(stdout); */
//         /* Man page says to not attempt to free the struct. */
//         return(theGid);
//     }
// }
//
// /* Wrap the chown system call: */
// /* Return NULL/0 if successful, a string for failure. */
// const char * wrapChown( const char * pathName, int uid, int gid) {
//     /* printf( "C DEBUG: pathName: %s\n", pathName); */
//     /* printf( "C DEBUG: uid: %d\n", uid); */
//     /* printf( "C DEBUG: gid: %d\n", gid); */
//     int status = chown( pathName, uid, gid);
//     /* printf( "C DEBUG: status: %d\n", status); */
//     if (0 == status) {
//         return(NULL);
//     }
//     /* Try to return an error string: */
//     const char * errorString;
//     int catchErrno = errno;
//     errorString = strerror(catchErrno);
//     if (NULL == errorString) {
//         /* This may not be necessary, but just in case... */
//         /* This string constant should be available in Go-land. */
//         return("The chown() call failed, but strerror() also failed.");
//     }
//     else {
//         /* Man page does not say whether this needs to be freed. */
//         return(errorString);
//     }
// }
//
// /* Return NULL for comparison purposes. */
// const char * returnNull() {
//     return(NULL);
// }
//
import "C"

import (
    "fmt"
    "log"
    "os"
    "strconv"
    "strings"
    "unsafe"
)

//////////////////////////////////////////////////////////////////////////////

// Constants:

// The name of the group file:
const groupFile = "/etc/group"

// Delimiter within lines in group file:
const groupDelim = ":"

// Delimiters accepted on command line:
const commandDelimiters = ":."

//////////////////////////////////////////////////////////////////////////////

// Message printing utility functions:

// Set this to non-zero to enable debug printing:
const enableDebugPrint = 0
// const enableDebugPrint = 1

func printDebug(msg string) {
    if (0 == enableDebugPrint) {
        return
    }
    fmt.Printf( "DEBUG:     %s\n", msg)
}

func printInfo(msg string) {
    fmt.Printf( "     info: %s\n", msg)
}

func printWarning(msg string) {
    fmt.Printf( "  Warning: %s\n", msg)
}

// This function takes a string.
// This function does not return:
func printError(msg string) {
    fmt.Printf( "*** ERROR: %s\n", msg)
    os.Exit(1)
}

// This function takes an error.
// This function does not return:
func printFatal(err error) {
    log.Fatal(err)
    os.Exit(1)
}

// This function does not return:
func printUsageError(msg string) {
    fmt.Printf( "*** ERROR: %s\n\n", msg)
    printUsage()
    os.Exit(1)
}

// Print the usage message and return:
func printUsage() {
    fmt.Printf("usage: %s owner:group file ...\n", os.Args[0])
}

//////////////////////////////////////////////////////////////////////////////

// Split the command argument username from group name.
// Accept either ':' or '.' as delimiter.
// Return a string slice.
// Error out if splitting fails.
func splitUserGroup(arg string) []string {
    parts  := strings.Split( arg, ":")
    parts2 := strings.Split( arg, ".")
    if (len(parts2) > len(parts)) {
        parts = parts2
    }
    if (2 != len(parts)) {
        printError("Invalid user[:group] argument: " + arg)
        // That function does not return.
    }
    return(parts)
}

// Convert string user uname to numeric user ID:
// Error out if the user name does not exist.
func userNameToUid(userName string) int {
    theCString := C.CString(userName)
    theCUid := C.userNameToUid(theCString)
    theGoUid := int(theCUid)
    printDebug("the Go uid: " + strconv.Itoa(theGoUid))
    C.free(unsafe.Pointer(theCString))
    if (0 > theGoUid) {
        printError("Invalid username: " + userName)
        // That function does not return.
    }
    return(theGoUid)
}

// Convert string group name to numeric group ID:
// Error out if the group name does not exist.
func groupNameToGid (groupName string) int {
    theCString := C.CString(groupName)
    theCGid := C.groupNameToGid(theCString)
    theGoGid := int(theCGid)
    printDebug("the Go gid: " + strconv.Itoa(theGoGid))
    C.free(unsafe.Pointer(theCString))
    if (0 > theGoGid) {
        printError("Invalid groupname: " + groupName)
        // That function does not return.
    }
    return(theGoGid)
}

// Attempt to chown the file.  Error out on failure.
func attemptChown( pathName string, uid int, gid int) {
    theCPath := C.CString(pathName)
    theCuid  := C.int(uid)
    theCgid  := C.int(gid)
    theCResult := C.wrapChown( theCPath, theCuid, theCgid)
    theCNull   := C.returnNull()
    if (theCNull == theCResult) {
        // Success:
        return;
    } else {
        // Report the error:
        printError(C.GoString(theCResult))
        // That function does not return.
    }
    // Keep the compiler happy.
    return;
}

// Start here: 
func main() {
    if (3 > len(os.Args)) {
        printUsageError("Too few arguments.")
        // That function does no return.
    }
    userGroup := splitUserGroup(os.Args[1])
    userName  := userGroup[0]
    printDebug("user name: " + userName)
    uid := userNameToUid(userName)
    printDebug("uid: " + strconv.Itoa(uid))
    groupName := userGroup[1]
    printDebug("group name: " + userName)
    gid := groupNameToGid(groupName)
    printDebug("user name: " + userName)
    printDebug("uid: " + strconv.Itoa(uid))
    printDebug("group name: " + userName)
    printDebug("gid: " + strconv.Itoa(gid))
    fileArgs := os.Args[2:]
    for _, fn := range fileArgs {
        printDebug("filename: " + fn)
        attemptChown( fn, uid, gid)
    }
}

// The End.
