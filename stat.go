// stat.go

// This simple program shows how to use cgo to use the stat(2) system call.
//
// The demo-cgo-linux project is licensed under the GNU General Public
// License, version 2 or any later version.  See file LICENSE for details.

package main

//////////////////////////////////////////////////////////////////////////////

// The inner C Code called by the outer Golang code:

// #include <errno.h>
// #include <grp.h>
// #include <pwd.h>
// #include <stdio.h>
// #include <stdlib.h>
// #include <string.h>
// #include <sys/stat.h>
// #include <sys/sysmacros.h>
// #include <sys/types.h>
// #include <time.h>
// #include <unistd.h>
//
// /* Define this macro to enable debug printing: */
// /* #define enableDebugPrinting */
//
// /* If enabled, print debug messages: */
// #ifdef enableDebugPrinting
//
// #define debugPrint(...) printf(__VA_ARGS__)
//
// #else /* enableDebugPrinting */
//
// #define debugPrint(...)
//
// #endif /* enableDebugPrinting */
//
// /* String constant in case of user/group lookup failure: */
// const char * noneString = "(NONE)";
//
// /* Wrap the stat() system call: */
// /* Return a pointer to a static struct. */
//
// /* Do not attempt to free the returned structure. */
//
// const struct stat * wrapStat(const char * pathName) {
//
//   /* Static struct for return value: */
//   static struct stat theStatStruct;
//
//   debugPrint( "C DEBUG: pathName: %s\n", pathName);
//   fflush(stdout);
//   int status = stat( pathName, &theStatStruct);
//   debugPrint( "C DEBUG: status: %d\n", status);
//   fflush(stdout);
//   return(&theStatStruct);
// }
//
// /* Wrap the ENOENT macro: */
// int wrapENOENT() {
//   return(ENOENT);
// }
//
// /* Wrap the strerror library function: */
//
// /* The man page does not say whether the string should be freed. */
// /* Experimentation says it should _NOT_ be freed. */
//
// const char * wrapStrerror(int num) {
//   return(strerror(num));
// }
//
// /* Wrap the major() macro: */
// unsigned int wrapMajor(__dev_t cdev) {
//   return(major(cdev));
// }
//
// /* Wrap the minor() macro: */
// unsigned int wrapMinor(__dev_t cdev) {
//   return(minor(cdev));
// }
//
// /* Shorthand function to malloc space and build a string return value. */
// const char * mallocString(const char * source) {
//   char * dest;
//   int lengPlus;
//   fflush(stdout);
//   if (NULL == source) {
//     source = noneString;
//     debugPrint( "C DEBUG: writing string: noneString: %s\n", source);
//     fflush(stdout);
//   }
//   lengPlus = strlen(source) + 1;
//   dest = (char *)malloc(lengPlus);
//   if (NULL != dest) {
//     debugPrint("C DEBUG: writing string: about to copy\n");
//     fflush(stdout);
//     strncpy( dest, source, lengPlus);
//     debugPrint("C DEBUG: writing string: did copy\n");
//     fflush(stdout);
//   }
//   return(dest);
// }
//
// /* Wrap a function around the file type macros: */
//
// /* DO free the returned string. */
//
// const char * wrapFileType(__mode_t theFullMode) {
//   /* Because of the structure of the bit mask, test in this order: */
//   if (S_IFSOCK == (S_IFMT & theFullMode)) {
//     return(mallocString("socket"));
//   }
//   if (S_IFLNK == (S_IFMT & theFullMode)) {
//     return(mallocString("symbolic link"));
//   }
//   if (S_IFREG == (S_IFMT & theFullMode)) {
//     return(mallocString("regular file"));
//   }
//   if (S_IFBLK == (S_IFMT & theFullMode)) {
//     return(mallocString("block device"));
//   }
//   if (S_IFDIR == (S_IFMT & theFullMode)) {
//     return(mallocString("directory"));
//   }
//   if (S_IFCHR == (S_IFMT & theFullMode)) {
//     return(mallocString("character device"));
//   }
//   if (S_IFIFO == (S_IFMT & theFullMode)) {
//     return(mallocString("FIFO"));
//   }
//   return(mallocString("INVALID"));
// }
//
// /* Wrap a function around the macro that reveals only the file's mode: */
// int wrapOnlyMode(__mode_t theFullMode) {
//   return(theFullMode & ~ S_IFMT);
// }
//
// /* Wrap the getpwuid() function: */
//
// /* NOTE: The caller should free the returned string. */
//
// const char * wrapGetpwuid(int uid) {
//   struct passwd * thePasswdStruct;
//
//   /* The man page for getpwuid() does not say whether */
//   /* the caller should free the struct. */
//
//   debugPrint("C DEBUG: before getpwuid\n");
//   fflush(stdout);
//   thePasswdStruct = getpwuid(uid);
//   debugPrint("C DEBUG: after getpwuid\n");
//   fflush(stdout);
//   if (NULL != thePasswdStruct) {
//     return(mallocString(thePasswdStruct->pw_name));
//   }
//   else {
//     return(mallocString(noneString));
//   }
// }
//
// /* Wrap the getgrgid() function: */
//
// /* NOTE: The caller should free the returned string. */
//
// const char * wrapGetgrgid(int gid) {
//   struct group  * theGroupStruct;
//
//   /* The man pages for getpwuid() and getgrgid() do not say whether */
//   /* the caller should free the struct. */
//
//   debugPrint("C DEBUG: before getgrgid\n");
//   fflush(stdout);
//   theGroupStruct = getgrgid(gid);
//   debugPrint("C DEBUG: after getgrgid\n");
//   fflush(stdout);
//   if (NULL != theGroupStruct) {
//     return(mallocString(theGroupStruct->gr_name));
//   }
//   else {
//     return(mallocString(noneString));
//   }
// }
//
// /* Wrap the ctime() function, but remove the trailing newline: */
//
// /* NOTE: The man page does not say whether to free the string */
// /*       returned by ctime(). */
//
// /* This function mallocs the return string. */
//
// const char * wrapCtime(__time_t theTime) {
//   time_t intermedTime = theTime;
//   char * intermed = ctime(&intermedTime);
//   char * newline = intermed + strlen(intermed) - 1;
//   if ('\n' == *newline) {
//     *newline = '\0';
//   }
//   return(mallocString(intermed));
// }
//
import "C"

import (
  "fmt"
  "log"
  "os"
  "strings"
  "unsafe"
)

//////////////////////////////////////////////////////////////////////////////

// Message printing utility functions:

// Set this to non-zero to enable debug printing:
const enableDebugPrint = 0
// const enableDebugPrint = 1

func printDebug(msg string) {
  if (0 == enableDebugPrint) {
    return
  }
  fmt.Printf( "DEBUG:     %s\n", msg)
}

func printInfo(msg string) {
  fmt.Printf( "     info: %s\n", msg)
}

func printWarning(msg string) {
  fmt.Printf( "  Warning: %s\n", msg)
}

// This function takes a string.
// This function does not return:
func printError(msg string) {
  fmt.Printf( "*** ERROR: %s\n", msg)
  os.Exit(1)
}

// This function takes an error.
// This function does not return:
func printFatal(err error) {
  log.Fatal(err)
  os.Exit(1)
}

// This function does not return:
func printUsageError(msg string) {
  fmt.Printf( "*** ERROR: %s\n\n", msg)
  printUsage()
  os.Exit(1)
}

// Print the usage message and return:
func printUsage() {
  fmt.Printf("usage: %s file ...\n", os.Args[0])
}

//////////////////////////////////////////////////////////////////////////////

// Attempt to stat the file.
// May print a warning and return nil.
// Error out on failure.
func attemptStat( pathName string, exceptError string) * C.struct_stat {
  theCPath        := C.CString(pathName)
  theCResult, err := C.wrapStat(theCPath)
  printPtr        := fmt.Sprintf( "%p", theCResult)
  printType       := fmt.Sprintf( "%T", theCResult)
  printErr        := fmt.Sprintf( "%v", err)
  printDebug("C.wrapStat() call returned value " + printPtr)
  printDebug("C.wrapStat() call returned type " + printType)
  printDebug("C.wrapStat() call returned error " + printErr)
  C.free(unsafe.Pointer(theCPath))
  //
  // Special case: change "no such file or directory" to a warning.
  // Print the warning here and return nil.
  if ((nil != err) && strings.EqualFold( exceptError, printErr)) {
    // Conver to warning:
    fmt.Println("")
    printWarning("Path/name does not exist: " + pathName)
    fmt.Println("")
    return(nil)
  }
  //
  if (nil != err) {
    printDebug("C.wrapStat() returned error")
    printError(printErr)
    // That function does not return.
    // However, for good measure...
    return(nil)
  } else if (nil == theCResult) {
    // Report the error:
    printError("C.wrapStat() did not return an errno with nil data")
    // That function does not return.
    // However, for good measure...
    return(nil)
  } else {
    // Success:
    printDebug("C.wrapStat() call succeeded: " + pathName)
    return(theCResult)
  }
}

// Print the information in the stat structure.
func printStat( pathName string, theStat * C.struct_stat) {
  //
  // Raw returns:
  printDebug("   cdev: " + fmt.Sprintf( "%v", theStat.st_dev))
  printDebug("  inode: " + fmt.Sprintf( "%v", theStat.st_ino))
  printDebug("   mode: " + fmt.Sprintf( "%o", theStat.st_mode))
  printDebug("  nlink: " + fmt.Sprintf( "%v", theStat.st_nlink))
  printDebug("    uid: " + fmt.Sprintf( "%v", theStat.st_uid))
  printDebug("    gid: " + fmt.Sprintf( "%v", theStat.st_gid))
  printDebug("   rdev: " + fmt.Sprintf( "%v", theStat.st_rdev))
  printDebug("   size: " + fmt.Sprintf( "%v", theStat.st_size))
  printDebug("blksize: " + fmt.Sprintf( "%v", theStat.st_blksize))
  printDebug(" blocks: " + fmt.Sprintf( "%v", theStat.st_blocks))
  printDebug(" a time: " + fmt.Sprintf( "%v", theStat.st_atim))
  printDebug(" m time: " + fmt.Sprintf( "%v", theStat.st_mtim))
  printDebug(" c time: " + fmt.Sprintf( "%v", theStat.st_ctim))
  //
  // Post-process a few fields:
  majorDev := C.wrapMajor(theStat.st_dev)
  minorDev := C.wrapMinor(theStat.st_dev)
  typeStr  := C.wrapFileType(theStat.st_mode)
  onlyMode := C.wrapOnlyMode(theStat.st_mode)
  userC, err := C.wrapGetpwuid(C.int(theStat.st_uid))
  var username string
  if (nil != err) {
    username = "--invalid-user--"
  } else {
    username = C.GoString(userC)
  }
  groupC, err := C.wrapGetgrgid(C.int(theStat.st_gid))
  var groupname string
  if (nil != err) {
    groupname = "--invalid-group--"
  } else {
    groupname = C.GoString(groupC)
  }
  //
  // An embedded utility function to convert a C struct timespec to a Go
  // string for printing--wrapped in a function literal.
  timespecToString := func (arg C.struct_timespec) string {
    tempStr := C.wrapCtime(arg.tv_sec)
    highOrder := C.GoString(tempStr)
    C.free(unsafe.Pointer(tempStr))
    return(fmt.Sprintf( "%s plus %v nsec", highOrder, arg.tv_nsec))
  }
  //
  // A little prettier form:
  printInfo("Path/name: " + pathName)
  printInfo("")
  printInfo("Containing device major: " + fmt.Sprintf( "%d", majorDev))
  printInfo("Containing device minor: " + fmt.Sprintf( "%d", minorDev))
  printInfo("")
  printInfo("Inode: " + fmt.Sprintf( "%v", theStat.st_ino))
  printInfo("")
  printInfo("File type: " + C.GoString(typeStr))
  printInfo("File mode: " + fmt.Sprintf( "%o", onlyMode))
  printInfo("")
  printInfo("Number of links: " + fmt.Sprintf( "%d", theStat.st_nlink))
  printInfo("")
  printInfo("Owner/User: " + fmt.Sprintf( "%v", theStat.st_uid) +
            ", " + username)
  printInfo("")
  printInfo("Group: " + fmt.Sprintf( "%v", theStat.st_gid) + ", " + groupname)
  printInfo("")
  printInfo("(if special file) device ID: " +
            fmt.Sprintf( "%v", theStat.st_rdev))
  printInfo("")
  printInfo("Size in bytes: " + fmt.Sprintf( "%v", theStat.st_size))
  printInfo("")
  printInfo("Block size: " + fmt.Sprintf( "%v", theStat.st_blksize))
  printInfo("")
  printInfo("Block count: " + fmt.Sprintf( "%v", theStat.st_blocks))
  printInfo("")
  printInfo("Last access time: " + timespecToString(theStat.st_atim))
  printInfo("")
  printInfo("Last modification time: " + timespecToString(theStat.st_mtim))
  printInfo("")
  printInfo("Last status change time: " + timespecToString(theStat.st_ctim))
  //
  // Finally, free all the allocated memory.
  C.free(unsafe.Pointer(typeStr))
  C.free(unsafe.Pointer(userC))
  C.free(unsafe.Pointer(groupC))
  // Keep the compiler happy.
  return;
}

// Start here: 
func main() {
  if (2 > len(os.Args)) {
    printUsageError("Too few arguments.")
    // That function does no return.
  }
  //
  // One time, capture the exception error string:
  theCint := C.wrapENOENT()
  theCstring := C.wrapStrerror(theCint)
  noSuchFile := C.GoString(theCstring)
  // Don't attempt to free either theCint or theCstring.
  printDebug("noSuchFile: " + noSuchFile)
  //
  // Now, iterate through the arguments:
  fileArgs := os.Args[1:]
  for _, fn := range fileArgs {
    fmt.Println("")
    printDebug("filename: " + fn)
    gotStat := attemptStat( fn, noSuchFile)
    if (nil != gotStat) {
      printStat( fn, gotStat)
      // Do not attempt to free the gotStat.
    }
  }
  fmt.Println("")
}

// The End.
