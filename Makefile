# Makefile for the demo-cgo-linux directory:

GOBUILD := go build

# Maintain this list of source files:
SOURCES := chown.go

# Maintain this list of object files:
OBJ     := chown

.PHONY: all clean

all: $(OBJ)

clean:
	rm -f $(OBJ)

$(OBJ): % : %.go
	$(GOBUILD) $<

# The End.
